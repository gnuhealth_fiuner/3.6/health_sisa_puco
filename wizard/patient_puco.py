# -*- coding: utf-8 -*-
#This file is part health_sisa_puco module for Tryton.
#The COPYRIGHT file at the top level of this repository contains
#the full copyright notices and license terms.
from ..puco_ws import PucoWS

from trytond.model import ModelView, fields
from trytond.wizard import Wizard, StateView, StateTransition, Button
from trytond.pool import Pool
from trytond.transaction import Transaction

__all__ = ['PatientPucoDataStart', 'PatientPucoData']


class PatientPucoDataStart(ModelView):
    'Patient PUCO Data Start'
    __name__ = 'patient.puco_data.start'

    cobertura_social = fields.Char('Insurance party', readonly=True)
    rnos = fields.Char('RNOS code', readonly=True)
    numero = fields.Char('Number')
    denominacion = fields.Char('Name', readonly=True)
    tipodoc = fields.Char('ID type', readonly=True)
    nrodoc = fields.Char('ID number', readonly=True)


class PatientPucoData(Wizard):
    'Patient PUCO Data'
    __name__ = 'patient.puco_data'

    @classmethod
    def __setup__(cls):
        super(PatientPucoData, cls).__setup__()
        cls._error_messages.update({
            'more_than_one_patient': 'Please, select only one patient',
            'error_ws': 'SISA web service error',
            'error_autenticacion': 'User authentication error',
            'error_inesperado': 'Unexpected error',
            'no_cuota_disponible': 'User has no asigned quota',
            'error_datos': 'Remote call error',
            'registro_no_encontrado': 'No data found',
            'multiple_resultado': 'More than one result',
        })

    start = StateView(
        'patient.puco_data.start',
        'health_sisa_puco.patient_puco_data_start_view', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Ok', 'update_patient', 'tryton-ok', default=True),
        ])
    update_patient = StateTransition()

    def default_start(self, fields):
        Patient = Pool().get('gnuhealth.patient')

        res = {}
        if len(Transaction().context['active_ids']) > 1:
            self.raise_user_error('more_than_one_patient')
        patient = Patient(Transaction().context['active_id'])
        if not patient.name:
            return res
        xml = PucoWS.get_xml(patient.name.ref)
        if xml is None:
            self.raise_user_error('error_ws')
        if xml.findtext('resultado') == 'OK':
            cobertura = []
            osocial = {}
            for cober in xml.findall('puco'):
                osocial['nombreObraSocial'] = cober.findtext('coberturaSocial')
                osocial['rnos'] = cober.findtext('rnos')
                osocial['denominacion'] = cober.findtext('denominacion')
                osocial['tipodoc'] = cober.findtext('tipodoc')
                osocial['nrodoc'] = cober.findtext('nrodoc')
                osocial['procedencia'] = 'puco'
                cobertura.append(osocial)
                osocial = {}    
            res = {
                'cobertura_social': cobertura[0]['nombreObraSocial'],
                'rnos': cobertura[0]['rnos'],
                'denominacion': cobertura[0]['denominacion'],
                'tipodoc': cobertura[0]['tipodoc'],
                'nrodoc': cobertura[0]['nrodoc'],
            }
        elif xml.findtext('resultado') == 'ERROR_AUTENTICACION':
            self.raise_user_error('error_autenticacion')
        elif xml.findtext('resultado') == 'ERROR_INESPERADO':
            self.raise_user_error('error_inesperado')
        elif xml.findtext('resultado') == 'NO_TIENE_QUOTA_DISPONIBLE':
            self.raise_user_error('no_cuota_disponible')
        elif xml.findtext('resultado') == 'ERROR_DATOS':
            self.raise_user_error('error_datos')
        elif xml.findtext('resultado') == 'REGISTRO_NO_ENCONTRADO':
            self.raise_user_error('registro_no_encontrado')
        elif xml.findtext('resultado') == 'MULTIPLE_RESULTADO':
            self.raise_user_error('multiple_resultado')

        return res

    def transition_update_patient(self):
        pool = Pool()
        Party = pool.get('party.party')
        Insurance = pool.get('gnuhealth.insurance')
        Patient = pool.get('gnuhealth.patient')

        patient = Patient(Transaction().context['active_id'])
        party = patient.name
        numero = self.start.numero if self.start.numero else self.start.nrodoc

        insurance_party = Party().search([
            ('is_insurance_company', '=', True),
            ('identifiers.type', '=', 'ar_rnos'),
            ('identifiers.code', '=', self.start.rnos),
            ])
        if not insurance_party:
            insurance_party = Party().search([
                ('is_insurance_company', '=', True),
                ('name', '=', self.start.cobertura_social),
                ])

        if insurance_party:
            insurance = Insurance().search([
                ('name', '=', party.id),
                ('company', '=', insurance_party[0].id),
                ])
            if not insurance:
                insurance_data = {
                    'name': party.id,
                    'number': numero,
                    'company': insurance_party[0].id,
                    'insurance_type':
                        insurance_party[0].insurance_company_type,
                    }
                insurance = Insurance.create([insurance_data])

            if insurance and not patient.current_insurance:
                patient.current_insurance = insurance[0]
                patient.save()

        return 'end'
